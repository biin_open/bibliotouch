var Vue = require('vue');
var VueLazyLoad = require('vue-lazyload');
var requestp = require('request-promise-native');
var packedThemeMapMixin = require('./mixins/packedThemeMap');
var ZoomHandler = require('./helpers/pinchToZoomHandler');
require('./components/borderIndicators');
require('./components/searchBox');

/**
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module
 */

//These values are used by the bin-packing algorithm (packedThemeMap.js)
//Apply changes to the CSS classes outer-map-*
//Sorry
var bookcellHeight = 65,
    bookcellWidth = 35,
    bookcoverHeight = 35,
    bookcoverWidth = 5;
    
Vue.use(VueLazyLoad, {
    preLoad : 1.2,
    lazyComponent : true
});

/**
 * Component displaying a single book spine
 */
var BookElement = {
    template : `<div class="outer-map-book-cell">
                    <img    class="outer-map-book-cover"
                            v-bind:src="generatedCoverSrc">
                    </img>
                </div>`,
    computed: {
        generatedCoverSrc : function(){
            let rnd = Math.trunc((Math.random()*7)+1);
            return `/res/covers/cover_${rnd}.png`;
        }
    }
}

/**
 * Component displaying books for a given theme
 * 
 * @property {string} theme - The theme of the component
 * @property {Number} biggestNbDocs - The biggest number of documents across the themes
 * @property {Number} ratio - Scale factor of the component 
 */
var ThemeWrapper = {
    template : `
                    <lazy-component 
                                @show="loadBooks"
                                v-bind:style="{
                                    position : 'absolute',
                                    left : theme.fit.x + 'px',
                                    top : theme.fit.y + 'px',
                                    width : theme.w + 'px',
                                    height : theme.h + 'px',         
                                    userSelect: 'none'}">
                        <div class="theme-element"
                            v-on:click="$router.push('/inner-theme-map/'+theme.name)">
                            <img class="enssib-deco" src="https://i.postimg.cc/rzX6MzMz/enssib-deco.png"/>
                            <div class="theme-cover"
                                :style="{
                                    backgroundColor: color
                                }"></div>
                            <div class="theme-cover-opaq"></div>
                            <div class="theme-lvl1"
                                :style="{
                                    backgroundColor: color
                                }"
                                v-if="theme.nbBooks > 500"></div>
                            <div class="theme-lvl2"
                                :style="{
                                    backgroundColor: color
                                }"
                                v-if="theme.nbBooks > 1000"></div>
                            <div class="theme-name"
                                v-bind:style="{
                                    fontFamily: 'Montserrat',
                                    fontWeight: '600',
                                    fontSize: '18px',
                                    textTransform : 'capitalize',
                                    color: '#000000',
                                    letterSpacing: '0',
                                    margin: '4px',
                                    textAlign: 'center',
                                    width: '140px'}">
                                {{theme.name}}
                            </div>
                            <p  class="theme-nb-docs">
                                {{theme.nbBooks}} documents
                            </p>
                        </div>
                    </lazy-component>`,
    props : ['theme', 'biggestNbDocs', 'smallestNbDocs', 'ratio'],
    data : function () {
        return {
            books : [],
            color: ''
        }
    },
    created: function () {
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        let colors = ['#2ee4c9','#50bbff','#6b85e4','#d6d6d6','#e38864','#cd5334','#e7e247'];
        this.color = colors[getRandomInt(0,colors.length-1)];
    },
    computed : {
        themeFontSize : function(){
            let maxSize = 58, minSize = 22;
            return Math.trunc(minSize + ((this.theme.nbBooks - this.smallestNbDocs)/(this.biggestNbDocs-this.smallestNbDocs))*(maxSize-minSize));
        },
        enterThemeButtonSize : function(){
        let maxSize = 48, minSize = 32;
            return Math.trunc(minSize + ((this.theme.nbBooks - this.smallestNbDocs)/(this.biggestNbDocs-this.smallestNbDocs))*(maxSize-minSize));    
        }
    },
    methods : {
        /**
         * Sets to itself an empty Array according to ratio
         * 
         * @param {any} component 
         */
        loadBooks : function(component){
            this.books = new Array(this.ratio > 0 ? Math.trunc(this.theme.nbBooks/this.ratio) : this.theme.nbBooks);
        }
    },
    components : {
        'book-element' : BookElement
    }
};

/**
 * Component display a map of themes, based on the PackedThemeMapMixin mixin
 */
var ThemeMap = Vue.extend({
    template : `<div id="outer-theme-map">
                    <theme-wrapper  v-for="theme in cthemes" 
                                    v-bind:key="theme.id"
                                    v-bind:theme="theme"
                                    v-bind:biggestNbDocs="biggestNbDocs"
                                    v-bind:smallestNbDocs="smallestNbDocs"
                                    v-bind:ratio="ratio">
                    </theme-wrapper>
                    <div   v-bind:style="{
                                    position : 'fixed',
                                    left: '50%',
                                    top: '50%',
                                    transform: 'translate(-50%, -50%)',
                                    fontFamily : 'Montserrat',
                                    color: '#BBBBBB',
                                    zIndex : '-1',         
                                    userSelect: 'none'
                                }">
                        <p  v-bind:style="{
                                    fontSize : '9vw',
                                    letterSpacing: '0',
                                    lineHeight: '200px',
                                    fontWeight: '700',
                                    margin : '0px',
                                    zIndex: '-15'
                                    }">
                            bibliotouch
                            <img src="/res/enssib-logo.svg" class="company-logo" />
                        </p>
                    </div>
                </div>`,
    mixins : [packedThemeMapMixin],
    data : function(){
        return {
            cthemes : [],
            error : null,
            loading : false,
            currentTheme : '',
            nbBooks : 0,
            bookcellHeight : bookcellHeight,
            bookcellWidth : bookcellWidth,
            ratio : 4,
            mapSize : null,
            zoomHandler : null,
            nbOfThemes: 0
        }
    },
    mounted: function(){
        let self = this;

        let zoomInHandler = function (x,y) {
            let element = self.getThemeElementFromPos(x,y);
            if(element) {
                self.$router.push('/inner-theme-map/'+element.name);
            } else {
                self.$router.push('/inner-theme-map/'+self.currentTheme);
            }
        }

        self.zoomHandler = new ZoomHandler(document.getElementById('outer-theme-map'));
        self.zoomHandler.setZoomHandlers(zoomInHandler,()=>{});
    },
    beforeDestroy: function(){
        this.zoomHandler.removeZoomHandlers();
    },
    components : {
        'theme-wrapper' : ThemeWrapper
    }
});

module.exports = ThemeMap;