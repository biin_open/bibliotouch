var Vue = require('vue');
var VueLazyLoad = require('vue-lazyload');
var gridDispatcher = require('./helpers/fixedGridDispatcher');
var requestp = require('request-promise-native');
var mouseDragScroll = require('./helpers/mouseDragScroll');
var ZoomHandler = require('./helpers/pinchToZoomHandler');
require('./components/searchBox');
var themeOrderer = require('./helpers/themeOrderer');

/**
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module
 */

var eventBus = new Vue();

//These values are used by the bin-packing algorithm (packedThemeMap.js)
//Apply changes to the CSS classes inner-map-*
//Sorry
var bookcellHeight = 168+60,
    bookcellWidth = 184+60,
    bookcoverHeight = 168,
    bookcoverWidth = 100;

Vue.use(VueLazyLoad, {
    preLoad : 1.2,
    lazyComponent : true
});

/**
 * Component displaying a single book cover and a cartouche with its title, author, etc...
 * @property {Object} book - The book displayed by the component
 */
var BookElement = {
    template : `<!--<lazy-component @show="setOnScreen">-->
                    <div v-bind:style="{
                        left : book.dispatch.x + 'px',
                        top : book.dispatch.y + 'px'}"
                        class="inner-map-book-cell">
                        <div    v-if="!imgAvailable"
                                class="inner-map-book-cover"
                                v-bind:style="{backgroundColor : randomColor}"
                                v-on:mousedown="initiateShowBookDetail"
                                v-on:mousemove="invalidateShowBookDetail"
                                v-on:mouseup="showBookDetail">
                                <div class="inner-map-book-cell-left-deco"></div>
                                <p  v-if="!imgAvailable"
                                    class="inner-map-generated-book-cover-title"
                                    v-on:mousedown="initiateShowBookDetail"
                                    v-on:mousemove="invalidateShowBookDetail"
                                    v-on:mouseup="showBookDetail">
                                    {{book.title}}
                                </p>
                                <div class="inner-map-book-cell-bottom-deco"></div>
                        </div>
                        <lazy-component v-if="book.hasCover"
                                        @show="loadCover" >
                                        
                            <transition name="fade">
                                <img    v-if="imgAvailable"
                                        class="inner-map-book-cover"
                                        v-bind:src="imgSrc"
                                        v-on:mousedown="initiateShowBookDetail"
                                        v-on:mousemove="invalidateShowBookDetail"
                                        v-on:mouseup="showBookDetail">
                                </img>
                            </transition>
                        </lazy-component>
                        <!--<div    class="cartouche-box"
                                v-on:mousedown="initiateShowBookDetail"
                                v-on:mousemove="invalidateShowBookDetail"
                                v-on:mouseup="showBookDetail">
                            <p  class="cartouche-title"
                                v-bind:id="titleId">
                                {{book.title}}
                            </p>
                            <p  class="cartouche-ellipsis"
                                v-if="isOverflown">
                                ...
                            </p>
                            <p  class="cartouche-author"
                                v-bind:id="authorsId"
                                v-if="bookloaded">
                                {{firstAuthor}}
                            </p>
                            <p  class="cartouche-date"
                                v-if="bookloaded">
                                {{parsedDatePub}}
                            </p>
                        </div>-->
                    </div>
                <!--</lazy-component>-->`,
    props : ['book'],
    data : function(){
        return {
            onScreen : false,
            imgAvailable : false,
            imgSrc : '',
            isOverflown : false,
            zoomHandler: null,
            bookloaded: true
        }
    },
    mounted: function(){
        //let element = document.getElementById(this.titleId);
        //this.isOverflown = element.scrollHeight > element.clientHeight || element.scrollWidth > element.clientWidth;
    },
    computed: {
        /**
         * Returns an URL to a random generated book cover
         * 
         * @returns {string} - the URL of a random book cover
         */
        generatedCoverSrc : function(){
            let rnd = Math.trunc((Math.random()*7)+1);
            return `/res/covers/cover_${rnd}.png`;
        },
        /**
         * Returns a random color from an inner palette
         * @return {string} - Color code
         */
        randomColor : function(){
            //Returns a color depending on the field of the search term
            //Old functionment : returning a random color, now we return a color corresponding to the type of field (author, authority, title)
            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }
            let colors = ['#2ee4c9','#50bbff','#6b85e4','#d6d6d6','#e38864','#cd5334','#e7e247'];
            return colors[getRandomInt(0,colors.length-1)];
        },
        authorsId : function(){
            return `${this.book.id}authors`
        },
        titleId : function(){
            return `${this.book.id}title`;
        },
        firstAuthor : function(){
            if(!this.book.authors) return '';
            return this.book.authors.length >= 1 ? this.book.authors[0] : '';
        },
        /**
         * Returns the first four digit year found in the datePub field of the book
         * 
         * @returns {string} - (Hopefully) the publication date of the book
         */
        parsedDatePub: function(){
            if(!this.book.datePub) return '';
            let matches = this.book.datePub.match(/\d{4}/);
            return (matches && matches.length >= 1) ? matches[0] : '';
        }
    },
    methods:{
        /**
         * Loads the book cover from the ISBN number of the book and applies it to the component
         * 
         * @param {HTMLElement} component - The element that triggered the call to this function (unsued, why do I keep this ?)
         */
        loadCover: function(component){
            let self = this;
            let isbn = this.book.isbn;
            if(isbn){
                requestp(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/covers/isbn/${isbn}-100.jpg`)
                    .then(function(body){
                        self.imgAvailable = true;
                        self.imgSrc = './covers/isbn/'+isbn+'-100.jpg';
                    })
                    .catch(function(err){});
            }
            /*
            requestp(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/book/${self.book.id}`)
                .then(function(body){
                    let respArray = JSON.parse(body);
                    if(respArray.length > 0) {
                        for (var attrname in respArray[0]) { self.book[attrname] = respArray[0][attrname]; }
                        self.bookloaded = true;
                        if(self.book.isbn && self.book.hasCover){
                            requestp(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/covers/isbn/${self.book.isbn}-56.jpg`)
                                .then(function(body){
                                    self.imgAvailable = true;
                                    self.imgSrc = './covers/isbn/'+self.book.isbn+'-56.jpg';
                                })
                                .catch(function(err){});
                        }
                    }
                })
                .catch(function(err){});
            */
        },
        setOnScreen : function(component){
            this.onScreen = true;
        },
        initiateShowBookDetail : function () {
            this.moved = false;
        },
        invalidateShowBookDetail : function(){
            if(!this.moved){
                this.moved = true;
            }
        },
        /**
         * Fires the event to show the detail modal for the book
         * @fires show-book-detail
         */
        showBookDetail : function(){
            if(!this.moved){
                /**
                 * @event show-book-detail
                 * @type {Object}
                 */
                eventBus.$emit('show-book-detail', this.book);
            }
        }
    }
}

/**
 * Component displaying all the books for a theme or a search in a 2D square grid
 */
var InnerThemeMap = Vue.extend({
    template : `<div id="inner-theme-map">
                    <img src="/res/rolling.gif" v-if="books.length == 0" class="load-rolling-spinner"/>
                    <book-element
                            v-for="book in books"
                            v-bind:key="book.id"
                            v-bind:book="book">
                    </book-element>
                    <div   v-bind:style="{
                                    position : 'fixed',
                                    left: '50%',
                                    top: '50%',
                                    transform: 'translate(-50%, -50%)',
                                    fontFamily : 'Montserrat',
                                    color: '#bbbbbb',
                                    zIndex : '-1',         
                                    userSelect: 'none'
                                }">
                        <p  v-bind:style="{
                                    fontSize : '150px',
                                    textTransform : 'capitalize',
                                    color: '#bbbbbb',
                                    letterSpacing: '0',
                                    lineHeight: '200px',
                                    margin : '0px'
                                    }">
                            {{theme}}
                        </p>
                        <p  v-if="books.length > 0 && centered == false"
                            v-bind:style="{
                                    fontSize : '30px',
                                    lineHeight: '15px',
                                    margin : '0',
                                    textAlign: 'center'
                                    }">
                            {{books.length}} documents
                        </p>
                    </div>
                </div>`,
    data : function () {
        return {
            books : [],
            theme : '',
            scrollHeight : 0,
            rows : 0,
            centered: false,
            order: themeOrderer
        }
    },
    watch: {
        '$route' (to, from) {
            if(to.path.match(/^\/inner-theme-map/)){
                this.populateMapFromTheme(to.params.theme_id);
            } else if(to.path.match(/^\/search-map/)) {
                this.populateMapFromQuery(to.params.query);
            }
        },
        'order.order' (newOrder, oldOrder) {
            switch (newOrder) {
                case themeOrderer.OrderOperator.Pertinence:
                    break;
                case themeOrderer.OrderOperator.AuthAsc:
                    this.orderThemeMap((a, b) => {
                        if(!a.authors[0]) return -1
                        if(!b.authors[0]) return 1
                        // The ordering of authors should happen on family name
                        let aArray = a.authors[0].split(' ');
                        let aname = aArray[aArray.length - 1]
                        let bArray = b.authors[0].split(' ');
                        let bname = bArray[bArray.length - 1]
                        return aname.localeCompare(bname);
                    })
                    break;
                case themeOrderer.OrderOperator.AuthDesc:
                    this.orderThemeMap((a, b) => {
                        if(!a.authors[0]) return -1
                        if(!b.authors[0]) return 1
                        // The ordering of authors should happen on family name
                        let aArray = a.authors[0].split(' ');
                        let aname = aArray[aArray.length - 1]
                        let bArray = b.authors[0].split(' ');
                        let bname = bArray[bArray.length - 1]
                        return bname.localeCompare(aname);
                    })
                    break;
                case themeOrderer.OrderOperator.TitleAsc:
                    this.orderThemeMap((a, b) => {
                        if(!a.title) return -1
                        return a.title.localeCompare(b.title);
                    })
                    break;
                case themeOrderer.OrderOperator.TitleDesc:
                    this.orderThemeMap((a, b) => {
                        if(!b.title) return 1
                        return b.title.localeCompare(a.title);
                    })
                    break;
                case themeOrderer.OrderOperator.DateAsc:
                    this.orderThemeMap((a, b) => {
                        if(!a.datePub) return -1
                        return a.datePub.localeCompare(b.datePub);
                    })
                    break;
                case themeOrderer.OrderOperator.DateDesc:
                    this.orderThemeMap((a, b) => {
                        if(!b.datePub) return 1
                        return b.datePub.localeCompare(a.datePub);
                    })
                    break;
                default:
                    break;
            }
        }
    },
    mounted : function(component){
        themeOrderer.order = themeOrderer.OrderOperator.Pertinence
        let self = this;
        this.theme = this.$route.params.theme_id || '';
        eventBus.$on('show-book-detail',(book)=>{self.$emit('show-book-detail', book)})
        if(this.$route.path.match(/^\/inner-theme-map/)){
            this.populateMapFromTheme(this.theme);
        } else if(this.$route.path.match(/^\/search-map/)) {
            this.populateMapFromQuery(this.$route.params.query);
        }
        mouseDragScroll.enableDragScroll();
        this.zoomHandler = new ZoomHandler(document.getElementById('inner-theme-map'));
        this.zoomHandler.setZoomHandlers(()=>{},()=>{self.$router.push(`/outer-theme-map`)});
    },
    beforeDestroy : function(){
        mouseDragScroll.disableDragScroll();
        this.zoomHandler.removeZoomHandlers();
    },
    components : {
        'book-element' : BookElement
    },
    updated: function () {
            document.querySelector('#inner-theme-map').style.height = `${(this.rows+1)*bookcellHeight}px`
    },
    methods : {
        /**
         * For a given theme, this function will get the according list of books and display them
         * 
         * @param {string} theme - The theme whose books we are loading
         * @fires current-theme-changed
         */
        populateMapFromTheme : function(theme){
            let self = this;
            //Remove currently charged books
            this.books.splice(0, this.books.length);
            requestp(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/themes/${theme}/books-objects`)
                .then(function(retrievedBooks){
                    let parsedBooks = JSON.parse(retrievedBooks);
                    let larg = Math.trunc((Math.sqrt(parsedBooks.length)+1));
                    let haut = Math.trunc(parsedBooks.length/larg)+1;

                    self.rows = haut;

                    self.$emit('current-theme-changed',theme);

                    self.centered = gridDispatcher.dispatch(parsedBooks,larg, bookcellWidth, bookcellHeight, true);
                    self.books.push(...parsedBooks);
                });
        },
        /**
         * Sort the books depending on the orderfunc given in parameter
         * 
         * @param {Function} orderfunc - the sort function
         */
        orderThemeMap : function(orderFunc){
            let larg = Math.trunc((Math.sqrt(this.books.length)+1));
            let haut = Math.trunc(this.books.length/larg)+1;
            this.rows = haut;

            this.books.sort(orderFunc)

            this.centered = gridDispatcher.dispatch(this.books,larg, bookcellWidth, bookcellHeight, true);
        },
        /**
         * For a given query, this function will get the according list of books and display them
         * 
         * @param {Array} query - The query used to load books
         * @fires current-theme-changed
         */
        populateMapFromQuery : function(query){
            let self = this;
            //Remove currently charged books
            this.books.splice(0, this.books.length);

            let queryOptions = {
                method: 'POST',
                uri: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/search/`,
                body: {
                    query: query
                },
                json: true
            };
    
            requestp(queryOptions)
                .then(function(retrievedBooks){
                    let parsedBooks = [];
                    for(let result of retrievedBooks){
                        parsedBooks.push(result.document);
                    }
                    let larg = Math.trunc((Math.sqrt(parsedBooks.length)+1));
                    let haut = Math.trunc(parsedBooks.length/larg)+1;

                    self.rows = haut;

                    //console.log('stop')
                    self.theme = ''
                    self.centered = gridDispatcher.dispatch(parsedBooks,larg, bookcellWidth, bookcellHeight, true);
                    self.books.push(...parsedBooks);
                });
        }
    }
});

module.exports = InnerThemeMap;