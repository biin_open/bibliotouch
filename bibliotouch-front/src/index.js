var Vue = require('vue');
var VueRouter = require('vue-router');
var VueMatomo = require('vue-matomo');

/**
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module
 */

Vue.use(VueRouter);

require('./components/searchBox');
require('./components/zoomNavBox');
require('./components/activeThemeBox');
require('./components/bookDetail');
require('./components/searchQueryBuilder');
require('./components/favCollectionButton');
require('./components/favCollection');

var ThemeMap = require('./themeMap');
var InnerThemeMap = require('./innerThemeMap');
var OuterThemeMap = require('./outerThemeMap');

//Routes
const routes = [
  { path: '/', component: OuterThemeMap},
  { path: '/outer-theme-map', component: OuterThemeMap},
  { path: '/outer-theme-map/:theme_id', component: OuterThemeMap},
  { path: '/theme-map', component: ThemeMap },
  { path: '/theme-map/:theme_id', component: ThemeMap },
  { path: '/inner-theme-map/:theme_id', component: InnerThemeMap},
  { path: '/search-map/:query', component: InnerThemeMap}
];

const router = new VueRouter({
  routes
})

/**
 * Setup of Matomo tracking
 */

let matomoUrl = document.querySelector('#matomo-setting').getAttribute('data-matomo-url')
let siteId = document.querySelector('#matomo-setting').getAttribute('data-site-id')

if(matomoUrl.length > 0 && siteId.length > 0){
  Vue.use(VueMatomo, {
    // Configure your matomo server and site
    host: matomoUrl,
    siteId: parseInt(siteId),

    // Enables automatically registering pageviews on the router
    router: router,

    // Require consent before sending tracking information to matomo
    // Default: false
    requireConsent: false,

    // Whether to track the initial page view
    // Default: true
    trackInitialView: true,

    // Changes the default .js and .php endpoint's filename
    // Default: 'piwik'
    trackerFileName: 'piwik'
  })
}

/**
 * Main Vue of the app
 */
const app = new Vue({
  router,
  data : {
    currentTheme : '',
    bookToShow: {},
    showBookModal: false,
    showSearchQueryBuilderModal: false,
    showFavStore: false,
    showThemeOrderer: false
  },
  methods : {
    toggleFavStore : function(){
      this.showFavStore = !this.showFavStore;
    },
    blurrAppContent : function(){
      document.getElementById('app-content').classList.add('blurred');
    },
    unblurrAppContent : function(){
      document.getElementById('app-content').classList.remove('blurred');
    },
    updateCurrentTheme : function(newTheme){
      this.currentTheme = newTheme;
      this.showThemeOrderer = false
    },
    showBookDetail : function(bookToShow){
      this.currentBook = bookToShow;
      this.showBookModal = true;
      this.blurrAppContent();
    },
    closeBookModal : function(){
      this.showBookModal = false;
      this.unblurrAppContent();
    },
    showSearchQueryBuilder : function(){
      this.showSearchQueryBuilderModal = true;
      this.blurrAppContent();
    },
    hideSearchQueryBuilder : function(){
      this.showSearchQueryBuilderModal = false;
      this.unblurrAppContent();
    },
    toggleThemeOrderer: function(){
      this.showThemeOrderer = !this.showThemeOrderer
    }
  }
}).$mount('#app')


//If we detect multitouch we don't want to trigger simple touch everywhere
window.addEventListener("touchstart", function (event){
    if(event.touches.length > 1){
        //the event is multi-touch
        event.preventDefault();
    }
});

window.addEventListener("touchmove", function (event){
    if(event.touches.length > 1){
        //the event is multi-touch
        event.preventDefault();
    }
});