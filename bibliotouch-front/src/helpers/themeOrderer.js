/**
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module
 */

/**
 * Tiny module to share ordering information of the innerthememap
 * @constructor
 */
var ThemeOrderer = function () {
  this.order = this.OrderOperator.Pertinence
}

/**
 * Enum for Order operator values
 * @readonly
 * @enum {number}
 */
ThemeOrderer.prototype.OrderOperator = Object.freeze({
    AuthAsc: 0,
    AuthDesc: 1,
    TitleAsc: 2,
    TitleDesc: 3,
    DateAsc: 4,
    DateDesc: 5,
    Pertinence: 6
});

ThemeOrderer.prototype.updateOrder = function (order) {
  console.log(order)
  this.order = order
}

module.exports = new ThemeOrderer();