/**
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module
 */

/**
 * A module giving xy coordinates to elements for a given number of columns
 * I doubt of its usefulness now
 * @constructor
 * 
 */
var FixedGridDispatcher = function () {
}

/**
 * Give to each element of elements a dispatch property with a x and a y value.
 * 
 * @param {Arry} elements - The array of elements to dispatch
 * @param {integer} columns - The number of columns on which we wish to dispatch
 * @param {integer} cellWidth - The width of and element
 * @param {integer} cellHeight - The height of an element
 * @returns {boolean} if the grid is centered because of too small or not
 */
FixedGridDispatcher.prototype.dispatch = function(elements, columns, cellWidth, cellHeight, center) {
    let i = 0;
    let xOffset = 0;
    let yOffset = 0;
    let centered = false;

    if(center === true) {
        console.log('stop')
        //If there is not enough elements to fill the screen, then center in the middle of the screen
        let gridWidth = columns*cellWidth;
        let gridHeight = (elements.length/columns)*cellHeight;

        if(gridWidth<window.innerWidth) {
            xOffset = (window.innerWidth - gridWidth) / 2;
            centered = true;
        }

        if(gridHeight<window.innerHeight) {
            yOffset = (window.innerHeight - gridHeight) / 2;
            centered = true;
        }
    }

    for(let element of elements) {
        element.dispatch = {
            x : ((i%columns)*cellWidth)+xOffset,
            y : (Math.trunc(i/columns)*cellHeight)+yOffset+1
        };
        i++;
    }

    return centered;
}


module.exports = new FixedGridDispatcher();