/**
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module
 */

/**
 * Tiny module to build queryArrays to be used by search-index module
 * @constructor
 */
var QueryBuilder = function () {}

/**
 * Enum for Boolean operator values
 * @readonly
 * @enum {number}
 */
QueryBuilder.prototype.BooleanOperator = Object.freeze({
    AND: 0,
    OR: 1,
    NOT: 2
});

/**
 * Returns a queryArray from an array of terms
 * @param {Array} terms - An array of term object (with fields 'field', 'text' and 'operator')
 * @return {Array} Array of query objects
 */
QueryBuilder.prototype.buildQuery = function(terms){
    let queryArray = [];
    let queryUnit = {AND:{},NOT:{}};
    let i = 0;
    for(let term of terms){
        if(term.operator === this.BooleanOperator.AND){
            if(!queryUnit.AND[term.field]) {
                queryUnit.AND[term.field] = [];
            }
            queryUnit.AND[term.field].push(term.text.toLowerCase());
        }
        if(term.operator === this.BooleanOperator.NOT){
            if(!queryUnit.NOT[term.field]) {
                queryUnit.NOT[term.field] = [];
            }
            queryUnit.NOT[term.field].push(term.text.toLowerCase());
        }
        if(term.operator === this.BooleanOperator.OR){
            // If the current term is an OR, we push all that was created earlier to the queryArray (except if it is the first term ever)
            if(i !== 0){
                queryArray.push(queryUnit);
            }
            // And then we create a new queryUnit
            queryUnit = {AND:{},NOT:{}};

            if(!queryUnit.AND[term.field]) {
                queryUnit.AND[term.field] = [];
            }
            queryUnit.AND[term.field].push(term.text.toLowerCase());
        }
        ++i
    }

    queryArray.push(queryUnit);
    return queryArray;
}

module.exports = new QueryBuilder();