var Vue = require('vue');
var VueLazyLoad = require('vue-lazyload');
var requestp = require('request-promise-native');
var gridDispatcher = require('./helpers/fixedGridDispatcher');
var ZoomHandler = require('./helpers/pinchToZoomHandler');
var packedThemeMapMixin = require('./mixins/packedThemeMap');
require('./components/borderIndicators');
require('./components/searchBox');

/**
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module
 */

var eventBus = new Vue();

//These values are used by the bin-packing algorithm (packedThemeMap.js)
//Apply changes to the CSS classes theme-map-*
//Sorry
var bookcellHeight = 140,
    bookcellWidth = 116,
    bookcoverHeight = 80,
    bookcoverWidth = 56;

Vue.use(VueLazyLoad, {
    preLoad : 1.2,
    lazyComponent : true
});

/**
 * Component displaying a single book cover
 * @property {Object} book - The book the component displays
 */
var BookElement = {
    template : `<div v-bind:style="{
                        left : book.dispatch.x + 'px',
                        top : book.dispatch.y + 'px'}"
                        class="theme-map-book-cell"
                        @show="setOnScreen">
                        <div    v-if="!imgAvailable"
                                class="theme-map-book-cover"
                                v-bind:style="{backgroundColor : randomColor}"
                                v-on:mousedown="initiateShowBookDetail"
                                v-on:mousemove="invalidateShowBookDetail"
                                v-on:mouseup="showBookDetail">
                                <p  v-if="bookloaded"
                                    class="theme-map-generated-book-cover-title"
                                    v-on:mousedown="initiateShowBookDetail"
                                    v-on:mousemove="invalidateShowBookDetail"
                                    v-on:mouseup="showBookDetail">
                                    {{book.title}}
                                </p>
                        </div>
                    <lazy-component 
                                    @show="loadCover">
                        <transition name="fade">
                            <img    v-if="imgAvailable"
                                    class="theme-map-book-cover"
                                    v-bind:src="imgSrc"
                                    v-on:mousedown="initiateShowBookDetail"
                                    v-on:mousemove="invalidateShowBookDetail"
                                    v-on:mouseup="showBookDetail">
                            </img>
                        </transition>
                    </lazy-component>
                </div>`,
    props : ['book'],
    data : function(){
        return {
            onScreen : false,
            imgAvailable : false,
            imgSrc : '',
            bookloaded: false
        }
    },
    computed: {
        generatedCoverSrc : function(){
            let rnd = Math.trunc((Math.random()*7)+1);
            return `/res/covers/cover_${rnd}.png`;
        },
        /**
         * Returns a random color from an inner palette
         * @return {string} - Color code
         */
        randomColor : function(){
            //Returns a color depending on the field of the search term
            //Old functionment : returning a random color, now we return a color corresponding to the type of field (author, authority, title)
            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }
            let colors = ['#2ee4c9','#50bbff','#6b85e4','#d6d6d6','#e38864','#cd5334','#e7e247'];
            return colors[getRandomInt(0,colors.length-1)];
        }
    },
    methods:{
        /**
         * Loads the cover of the book if available and sets it to the component
         * 
         * @param {any} component 
         */
        loadCover: function(component){
            let self = this;
            requestp(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/book/${self.book.id}`)
                .then(function(body){
                    let respArray = JSON.parse(body);
                    if(respArray.length > 0) {
                        for (var attrname in respArray[0]) { self.book[attrname] = respArray[0][attrname]; }
                        self.bookloaded = true;
                        if(self.book.isbn && self.book.hasCover){
                            requestp(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/covers/isbn/${self.book.isbn}-56.jpg`)
                                .then(function(body){
                                    self.imgAvailable = true;
                                    self.imgSrc = './covers/isbn/'+self.book.isbn+'-56.jpg';
                                })
                                .catch(function(err){});
                        }
                    }
                })
                .catch(function(err){});
        },
        setOnScreen : function(component){
            this.onScreen = true;
        },
        initiateShowBookDetail : function(){
            this.moved = false;
        },
        invalidateShowBookDetail : function(){
            if(!this.moved){
                this.moved = true;
            }
        },
        showBookDetail : function(){
            if(!this.moved){
                eventBus.$emit('show-book-detail', this.book);
            }
        }
    }
}

/**
 * Component displaying a grid of book covers
 * @property {string} theme - The theme of the component
 */
var ThemeWrapper = {
    template : `
                    <lazy-component 
                                @show="loadBooks"
                                @rest="unloadBooks"
                                @unrest="loadBooks"
                                v-bind:style="{
                                    position : 'absolute',
                                    left : theme.fit.x + 'px',
                                    top : theme.fit.y + 'px',
                                    width : theme.w + 'px',
                                    height : theme.h + 'px',         
                                    userSelect: 'none'}">
                        <img src="/res/rolling.gif" v-if="showSpinner" class="load-rolling-spinner"/>
                        <book-element
                                    v-for="book in books"
                                    v-bind:key="book.id"
                                    v-bind:book="book">
                        </book-element>
                    </lazy-component>`,
    props : ['theme'],
    data : function () {
        return {
            books : [],
            showSpinner: false
        }
    },
    methods : {
        /**
         * Retrieves books for the theme of the component and displays them once loaded
         * 
         * @param {any} component 
         */
        loadBooks : function(component){
            this.showSpinner = true;
            let self = this;
            requestp(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/themes/${component.$parent.theme.name}/books`)
                .then(function(retrievedBooks){
                    let parsedBooks = JSON.parse(retrievedBooks);
                    gridDispatcher.dispatch(parsedBooks,component.$parent.theme.w/bookcellWidth, bookcellWidth, bookcellHeight);
                    self.books.push(...parsedBooks);
                    self.showSpinner = false;
                });
        },
        unloadBooks: function(){
            this.books.splice(0, this.books.length);
        }
    },
    components : {
        'book-element' : BookElement
    }
};

/**
 * Vue displaying a bin-packed map of themes, using the PackedThemeMapMxin
 */
var ThemeMap = Vue.extend({
    template : `<div id="theme-map">
                    <theme-wrapper  v-for="theme in cthemes" 
                                    v-bind:key="theme.id"
                                    v-bind:theme="theme">
                    </theme-wrapper>
                    <div   v-bind:style="{
                                    position : 'fixed',
                                    left: '50%',
                                    top: '50%',
                                    transform: 'translate(-50%, -50%)',
                                    fontFamily : 'Montserrat',
                                    color: '#BBBBBB',
                                    zIndex : '-1',         
                                    userSelect: 'none'
                                }">
                        <p  v-bind:style="{
                                    fontSize : '150px',
                                    textTransform : 'capitalize',
                                    letterSpacing: '0',
                                    lineHeight: '200px',
                                    fontWeight: '700',
                                    margin : '0px'
                                    }">
                            {{currentTheme}}
                        </p>
                        <p  v-if="nbBooks > 0"
                            v-bind:style="{
                                    fontSize : '30px',
                                    lineHeight: '15px',
                                    margin : '0',
                                    color: '#D7D7D7',
                                    fontWeight: '600',
                                    textAlign: 'center'
                                    }">
                            {{nbBooks}} documents
                        </p>
                    </div>
                    <div    class="enter-theme-button"
                            v-on:click="$router.push('/inner-theme-map/'+currentTheme)"
                            v-bind:style="{
                                position:'fixed',
                                top: '608px'
                            }">
                        <img src="/res/arrow_right.png"/>
                    </div>
                    <border-indicators
                                        v-bind:topNeighbour="topNeighbour"
                                        v-bind:botNeighbour="botNeighbour"
                                        v-bind:leftNeighbour="leftNeighbour"
                                        v-bind:rightNeighbour="rightNeighbour">
                    </border-indicators>
                </div>`,
    mixins : [packedThemeMapMixin],
    data : function(){
        return {
            cthemes : [],
            error : null,
            loading : false,
            currentTheme : '',
            nbBooks : 0,
            bookcellHeight : bookcellHeight,
            bookcellWidth : bookcellWidth,
            mapSize : null,
            topNeighbour : null,
            botNeighbour : null,
            leftNeighbour : null,
            rightNeighbour : null,
            zoomHandler : null
        }
    },
    mounted: function(){
        let self = this;
        eventBus.$on('show-book-detail',(book)=>{self.$emit('show-book-detail', book)})

        let zoomInHandler = function (x,y) {
            let element = self.getThemeElementFromPos(x,y);
            if(element) {
                self.$router.push('/inner-theme-map/'+element.name);
            } else {
                self.$router.push('/inner-theme-map/'+self.currentTheme);
            }
        }

        let zoomOutHandler = function () {
            self.$router.push('/outer-theme-map/'+self.currentTheme);
        }
        self.zoomHandler = new ZoomHandler(document.getElementById('theme-map'));
        self.zoomHandler.setZoomHandlers(zoomInHandler,zoomOutHandler);
    },
    beforeDestroy: function(){
        this.zoomHandler.removeZoomHandlers();
    },
    components : {
        'theme-wrapper' : ThemeWrapper
    }
});

module.exports = ThemeMap;