var Vue = require('vue');
var favStore = require('../helpers/favStore')
var requestp = require('request-promise-native');

BookFavItem = {
  template : `
    <div class="fav-collection-item">
      <img v-if="book.hasCover" v-bind:src="imgSrc" class="fav-collection-img"/>
      <div v-else class="fav-collection-img" v-bind:style="{backgroundColor : randomColor}">
        <p>{{book.title}}</p>
      </div>
      <div class="fav-collection-text-wrapper">
        <div class="fav-collection-book-title">{{book.title}}</div>
        <div class="fav-collection-authors-wrapper">
            <span v-for="author in book.authors">{{author}} </span>
        </div>
        <div class="fav-collection-book-description">{{book.description}}</div>
      </div>
      <div class="fav-collection-trash-icon" v-on:click="trashBook"><img src="/res/pictoClose.svg"/></div>
    </div>
  `,
  props : ['book'],
  data : function () {
    return {
      imgSrc : ''
    }
  },
  created : function(){
          let rnd = Math.trunc((Math.random()*7)+1);
          this.imgSrc =  `/res/covers/cover_${rnd}.png`;
          this.getTrueCover();
  },
  computed : {
    /**
     * Returns a random color from an inner palette
     * @return {string} - Color code
     */
    randomColor : function(){
        //Returns a color depending on the field of the search term
        //Old functionment : returning a random color, now we return a color corresponding to the type of field (author, authority, title)
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        let colors = ['#2ee4c9','#50bbff','#6b85e4','#d6d6d6','#e38864','#cd5334','#e7e247'];
        return colors[getRandomInt(0,colors.length-1)];
    }
  },
  methods : {
    getTrueCover : function () {
      let self = this;
      let isbn = this.book.isbn;
      if(isbn){
          requestp(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/covers/isbn/${isbn}.jpg`)
              .then(function(body){
                  self.imgSrc = './covers/isbn/'+isbn+'.jpg';
              })
              .catch(function(err){});
      }
    },
    trashBook: function () {
      favStore.removeBook(this.book);
    }
  }
}

/**
 * Allows to diplay the favorites chosen by the user
 */
Vue.component('fav-collection', {
    template: ` <div id="fav-collection">
                  <div v-on:click="$emit('close-fav-store')" id="fav-collection-close-text"><img src="/res/cross_black.png" id="fav-collection-close-cross"/><span> Fermer</span></div>
                  <div id="fav-collection-box-title">Ma sélection <span id="fav-collection-counter">{{books.length}}</span></div>
                  <div v-if="favCollectionState === 0">
                    <div id="fav-collection-items-wrapper" v-on:scroll.once>
                      <div id="fav-collection-fade-top"></div>
                      <book-fav-item  v-for="book in books"
                                      v-bind:key="book.id"
                                      v-bind:book="book">
                      </book-fav-item>
                      <div id="fav-collection-fade-bottom"></div>
                    </div>
                    <div id="fav-collection-button-send-mail-div"><span class="fav-collection-button-black" v-on:click="books.length > 0 ? favCollectionState = 1 : null">Envoyer par mail</span></div>
                  </div>
                  <div v-if="favCollectionState === 1" class="fav-collection-state-wrapper">
                    <img src="/res/panier-avion.svg" class="fav-collection-big-svg" />
                    <p>{{getSendDocumentsSentence()}}</p>
                    <input type="email" placeholder="ADRESSE MAIL" id="fav-collection-email-input" v-model="emailaddress"/>
                    <span class="fav-collection-button-white" v-on:click="favCollectionState = 0">Annuler</span>
                    <span class="fav-collection-button-black" v-on:click="sendMail()" v-if="!isSending">Envoyer</span>
                    <span class="fav-collection-button-black" v-if="isSending">Envoi en cours...</span>
                  </div>
                  <div v-if="favCollectionState === 2" class="fav-collection-state-wrapper">
                    <img src="/res/panier-validation-envoi.svg" class="fav-collection-big-svg" />
                    <p>Votre mail a bien été envoyé à l'adresse suivante :</p>
                    <p id="fav-collection-email-result">{{emailaddress}}</p>
                    <span class="fav-collection-button-white" v-on:click="favCollectionState = 0">Retour</span><span class="fav-collection-button-black" v-on:click="resetFavs()">Effacer la sélection</span>
                  </div>
                  <div v-if="favCollectionState === 3" class="fav-collection-state-wrapper">
                    <img src="/res/panier-error.svg" class="fav-collection-big-svg" />
                    <p>Une erreur a eu lieu lors de l'envoi.</p>
                    <p id="fav-collection-email-result">Veuillez nous excuser pour la gêne et réessayer ulturieurement.</p>
                    <span class="fav-collection-button-white" v-on:click="favCollectionState = 0">Retour</span>
                  </div>
                </div>`,
    data: function () {
        return {
            books: favStore.books,
            favCollectionState: 0,
            emailaddress: '',
            isSending: false
        }
    },
    components : {
        'book-fav-item' : BookFavItem
    },
    created: function () {
      favCollectionState = 0;
    },
    methods: {
      getSendDocumentsSentence: function () {
        if(this.books.length === 1) {
          return "Entrez votre adresse mail pour envoyer votre document"
        } else {
          return `Entrez votre adresse mail pour envoyer vos ${this.books.length} documents`
        }
      },
      resetFavs : function () {
        favStore.books = []
        this.books = []
        this.favCollectionState = 0
      },
      sendMail : function () {
        if(this.emailaddress.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)) {
          this.isSending = true
          fetch('/sendmail', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({books:this.books, address: this.emailaddress})
          }).then((rawResponse) => {
            this.isSending = false
            if(rawResponse.status === 200) {
              this.favCollectionState = 2
            } else {
              this.favCollectionState = 3
            }
          })
        }
      }
    }
})