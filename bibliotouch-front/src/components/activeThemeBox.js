var Vue = require('vue');
var themeOrderer = require('../helpers/themeOrderer')
/**
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module
 */

Vue.component('inner-theme-orderer', {
    template: `
        <div id="inner-theme-orderer" v-if="showActiveTheme">
            <div class="orderer">
                <span class="orderer-label">Auteur</span>
                <span class="orderer-asc" :class="{'selected-orderer': themeOrderer.order == themeOrderer.OrderOperator.AuthAsc}" @click="updateThemeOrderer(themeOrderer.OrderOperator.AuthAsc)">A...Z</span>
                <span class="orderer-desc" :class="{'selected-orderer': themeOrderer.order == themeOrderer.OrderOperator.AuthDesc}" @click="updateThemeOrderer(themeOrderer.OrderOperator.AuthDesc)">Z...A</span>
            </div>
            <div class="orderer">
                <span class="orderer-label">Titre</span>
                <span class="orderer-asc" :class="{'selected-orderer': themeOrderer.order == themeOrderer.OrderOperator.TitleAsc}" @click="updateThemeOrderer(themeOrderer.OrderOperator.TitleAsc)">A...Z</span>
                <span class="orderer-desc" :class="{'selected-orderer': themeOrderer.order == themeOrderer.OrderOperator.TitleDesc}" @click="updateThemeOrderer(themeOrderer.OrderOperator.TitleDesc)">Z...A</span>
            </div>
            <div class="orderer">
                <span class="orderer-label">Date</span>
                <span class="orderer-asc" :class="{'selected-orderer': themeOrderer.order == themeOrderer.OrderOperator.DateAsc}" @click="updateThemeOrderer(themeOrderer.OrderOperator.DateAsc)">Anciens</span>
                <span class="orderer-desc" :class="{'selected-orderer': themeOrderer.order == themeOrderer.OrderOperator.DateDesc}" @click="updateThemeOrderer(themeOrderer.OrderOperator.DateDesc)">Récents</span>
            </div><!--
            <div class="orderer">
                <span class="orderer-label">Pertinence</span>
                <span class="orderer-asc" @click="updateThemeOrderer(themeOrderer.OrderOperator.Pertinence)">Rétablir</span>
            </div>-->
        </div>`,
        data: function () {
            return {
                themeOrderer: themeOrderer,
                showActiveTheme: true
            }
        },
        watch : {
            '$route' (to, from) {
                if(to.fullPath.match(/^\/inner-theme-map/)) {
                    this.showActiveTheme = true;
                } else {
                    this.showActiveTheme = false;
                }
            }
        },
        methods: {
            updateThemeOrderer: function(order) {
                themeOrderer.updateOrder(order)
            }
        }
})

/**
 * Floating button indicating the theme the user is currently consulting, only appear when using the inner-themem-map view
 */ 
Vue.component('active-theme-box', {
    template: `<div id="active-theme-box"
                    v-if="showActiveTheme">
                    <div class="return-box" v-on:click="$router.push('/outer-theme-map/')">
                        <span ></span>
                        <p id="active-theme-label">
                            Retour
                        </p>
                    </div>
                    <div class="order-button" @click="showOrderer()">
                    </div>
                </div>`,
    data : function() {
        return {
            showActiveTheme : false,
            currentTheme : ''
        }
    },
    created : function(){
        if(this.$route.fullPath.match(/^\/inner-theme-map/) || this.$route.fullPath.match(/^\/search-map/)) {
            this.showActiveTheme = true;
            this.currentTheme = this.$route.params.theme_id;
        }
    },
    watch : {
        '$route' (to, from) {
            if(to.fullPath.match(/^\/inner-theme-map/) ||to.path.match(/^\/search-map/)) {
                this.showActiveTheme = true;
                this.currentTheme = to.params.theme_id;
            } else {
                this.showActiveTheme = false;
                this.currentTheme = '';
            }
        }
    },
    methods: {
        showOrderer: function() {
            this.$emit('show-theme-orderer')
        }
    }
})