var Vue = require('vue');
var favStore = require('../helpers/favStore')

/**
 * Allows to diplay the favorites chosen by the user
 */
Vue.component('fav-collection-button', {
    template: `<div id="fav-collection-button" v-on:click="$emit('open-fav-store')">
                    <img src="res/pictoHeartFill.svg" id="fav-store-icon"></img>
                    <span id="fav-counter">
                        {{books.length}}
                    </span>
                </div>`,
    data: function () {
        return {
            books: favStore.books
        }
    }
})