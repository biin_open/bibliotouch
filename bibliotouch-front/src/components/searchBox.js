var Vue = require('vue');
var queryBuilder = require('../helpers/queryBuilder');
var stopword = require('stopword');

/**
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module
 */

/**
* Floating central bar showing the SearchQueryBuilder when clicked
*/
Vue.component('search-box', {
    template : `<div id="search-box"
                        v-on:click="showSearchQueryBuilder()">
                    <img id="search-icon"
                        alt="research"
                        src="/res/research.png"></img>
                    <p id="search-input">Je cherche...</p>
                    <div id="others-reading-div"
                            v-on:click.stop="showGiveMeAnIdea()">
                        Nouveautés
                    </div>
                </div>`,
    methods: {
        /**
         * @fires show-search-query-builder
         */
        showSearchQueryBuilder : function(){
            /**
             * @event show-search-query-builder - Show search query builder screen event
             */
            this.$emit('show-search-query-builder');
        },
        showGiveMeAnIdea: function(){
            let currentYear = `${new Date().getFullYear()}`
            let lastYear = `${new Date().getFullYear() - 1}`
            let splitAuthority = [lastYear, currentYear]
            let field = 'datePub'
            let termsArray = [];
            for(let auth of splitAuthority){
                termsArray.push({
                    field : field,
                    text : auth,
                    operator : queryBuilder.BooleanOperator.OR
                });
            }
            let queryArray = queryBuilder.buildQuery(termsArray);
            //console.log(queryArray)
            this.$router.push(`/search-map/${encodeURIComponent(JSON.stringify(queryArray))}`);
        
        }
    }
})