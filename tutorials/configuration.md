# Configuration

This configuration file is huge, the biggest part is relative to the parameters for the koha importer and defining which MARC field correspond to which search-index field.


    {
        "Bibliotouch" : {
            "koha":{
                "kohadbconfig" : {
                    "host" : "localhost",             // Hostname of the koha mysql database
                    "port" : "3306",                  // Port of the koha mysql database
                    "dbname" : "bibliotouch",         // The koha database name
                    "user" : "bibliotouch",           // The user name to access the koha database
                    "pwd" : "bibliotouch"             // The password for the aforementionned user
                },
                "oaipmhEndpoint" : {
                    "url" : "http://...",             // URL of the OAIPMH endpoint for daily update of the database
                    "update":false,                   // Defines if the server will update itself using the OAIPMH, while developing this can be set to false
                    "maxOAIPMHExports":3000           // The number of record we import at a time from the OAIPMH endpoint (if to high, the OAIPMH may block or return truncated results)
                },
                "restApiEndpoint" : "https://...",    // The URL to the REST API endpoint
                "kohaTags" :{
                    "isbn" : {                        // Here is the list of all the properties that are taken care of by search-index
                        "tag" : ["010"],              // For each property you can define a tag and a code corresponding to MARC format
                        "code": ["a"]
                    },
                    ...
                }
            }
            ,
            "updateTime":{
                "cron" : "0 4 * * *"                  // This cron string defines the recurrence of the updates 
            },
            "serverPort" : 8080,                      // The port where the bibliotouch app will be available from
            "downloadCovers" : false                  // Defines if the server downloads covers for each record. While developing it can be set to false
        }
    }
