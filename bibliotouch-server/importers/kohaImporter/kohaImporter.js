/**
 * There be dragons
 * 
 * Module enabling :
 * - initial import of records from the Koha database
 * - update of the search-index using the configured OAIPMH endpoint
 * 
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module
 */

var XmlStream = require('xml-stream');
var parseXmlString = require('xml2js').parseString;
var request = require('request-promise-native');
var fs = require('fs');
var path = require('path');
var Logger = require('../../helpers/logger');
var config = require('config');
var BiblioItems = require('./biblioitems');
var coverDownloader = require('../../helpers/coverDownloader');
var authorityManager = require('../../models/authorities');

var Readable = require('stream').Readable;

var batchSize = 3000;
var maxOAIPMHExports = config.get('Bibliotouch.koha.oaipmhEndpoint').maxOAIPMHExports;
var kohaTags = config.get('Bibliotouch.koha.kohaTags');
var lastUpdateFilename = 'data/lastUpdate.json';
var oaipmhEndpoint = config.get('Bibliotouch.koha.oaipmhEndpoint').url;
var weirdWordRegex = /([\w]*[áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]+[\w]*([áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]+[\w]*)*)/g;
var weirdA = /[áàâäãåÁÀÂÄÃÅ]/g;
var weirdC = /[çÇ]/g;
var weirdE = /[éèêëÉÈÊË]/g;
var weirdI = /[íìîïÍÌÎÏ]/g;
var weirdN = /[ñÑ]/g;
var weirdO = /[óòôöõÓÒÔÖÕ]/g;
var weirdU = /[úùûüÚÙÛÜ]/g;
var weirdY = /[ýÿÝŸ]/g;
var weirdAE = /[æÆ]/g;
var weirdOE = /[œŒ]/g;

var KohaImporter = function() {
}


/**
 * Returns an array of words where weird letters of the values of an object have been replaced with their more simple equivalent
 * 
 * @example
 * //returns ['poete','mais']
 * getNormalizedWords({a:'poète',b:'maïs'})
 * 
 * @param {Object} biblioObject - The object where we track weird letters
 * @returns {Array} - Normalized words
 */
var getNormalizedWords = function(biblioObject){
    let normalizedWords = [];
    JSON.stringify(biblioObject).replace(weirdWordRegex,function(match){
        let normalizedWord = match
                                .replace(weirdA,'a')
                                .replace(weirdAE,'ae')
                                .replace(weirdC,'c')
                                .replace(weirdE,'e')
                                .replace(weirdI,'i')
                                .replace(weirdN,'n')
                                .replace(weirdO,'o')
                                .replace(weirdOE,'oe')
                                .replace(weirdU,'u')
                                .replace(weirdY,'y');
        if(normalizedWords.indexOf(normalizedWord) == -1){
            normalizedWords.push(normalizedWord);
        }
    });
    return normalizedWords;
}

/**
 * Updates or create the file storing the last update date using the given date object or current time
 * 
 * @param {Date} [date] 
 */
var updateLastUpdateFile = function(date){
    let updateDate = date ||new Date();
    let importDate = {
        lastUpdate : updateDate.getTime()
    };

    fs.writeFile(lastUpdateFilename, JSON.stringify(importDate), function(err){
        if (err) {
            Logger.log('error',err);
        }
    });
}

/**
 * Return the most recent timestamp from 2 Dates
 * 
 * @param {Date} oldTimeStamp 
 * @param {Date} newTimeStamp 
 * @returns {Date}
 */
var getMostRecentTimestamp = function(oldTimeStamp, newTimeStamp){
    if(oldTimeStamp.getTime() < newTimeStamp.getTime()){
        return newTimeStamp;
    } else {
        return oldTimeStamp;
    }
}

/**
 * Import into the search index all the documents found in the configured database
 * 
 * This monstruously huge method notaby triggers a few things :
 * - update of the configuration object storing the last update
 * - flush and saving the Authorities
 * 
 * Few things to know :
 * - the reading of the Koha database is batched by 3000 (variable at the beginning of the doc to change it)
 * - here each MARCXML record is transformed into usable javascript object
 * 
 * @returns {Promise}
 */
KohaImporter.prototype.import = async function(){
    let self = this;
    var mostRecentTimeStamp = new Date(0);
    async function importPromise(resolve, reject){
        var recordStream = new Readable( {objectMode: true} )
        
        let nbDocs;
        try{
            nbDocs = await BiblioItems.count();
        } catch(e){
            reject(e);
            Logger.log('error',e);
            return;
        }
        let nbToProcess = nbDocs;
        let nbProcessed = 0;
        authorityManager.flushAuthorities();

        while (nbToProcess > 0) {
            let nbToRetrieve = nbToProcess >= batchSize ? batchSize : nbToProcess;
            nbToProcess -= nbToRetrieve;

            let instances;
            try{
                instances = await BiblioItems.findAll({limit:nbToRetrieve, offset:nbProcessed});
            } catch(e){
                console.log(e);
            }
            for (var index = 0; index < instances.length; index++) {
                var instance = instances[index];
                let record = {};
                try{
                    let jsonRecord = await new Promise(function(resolve, reject){
                        parseXmlString(instance.get('marcxml'),function(err, result){
                            if(err){
                                reject(err);
                            } else {
                                resolve(result);
                            }
                        })
                    });
                    record = await self.parseRecordObject(jsonRecord.record);
                    let mostRecentTimeStamp2 = getMostRecentTimestamp(mostRecentTimeStamp, instance.get('timestamp'));//Weird behaviour
                    mostRecentTimeStamp = mostRecentTimeStamp2;
                    recordStream.push(record);
                } catch(e){
                    console.log(e);
                }
                
            }
            nbProcessed += nbToRetrieve;
        }
        recordStream.push(null);

        updateLastUpdateFile(mostRecentTimeStamp);
        authorityManager.saveAuthorities();

        resolve(recordStream);
    }

    try {
        await BiblioItems.sync()
    } catch (err) {
        console.log(err);
        Logger.log('error',err);
    }

    return new Promise(importPromise);
}

/**
 * Updates the search index documents and add those that are new using the OAIPMH endpoint of Koha
 * 
 * The update starts from the date the search index was last update or the date of initial import 
 * 
 * @returns {Promise}
 */
KohaImporter.prototype.update = function(){
    var self = this;
    if(!config.get('Bibliotouch.koha.oaipmhEndpoint').update){
        return new Promise(()=>{});
    }
    
    async function updatePromise(resolve, reject){
        function requireUncached(module){
            delete require.cache[require.resolve(module)]
            return require(module)
        }

        var recordStream = new Readable( {objectMode: true} )
        var recordArray = []
        let importDate = requireUncached('../../'+lastUpdateFilename);
        let lastUpdate = new Date(importDate.lastUpdate);
        let today = new Date();
        let lastUpdateString = `${lastUpdate.getUTCFullYear()}-${(lastUpdate.getUTCMonth()+1) < 10 ? '0'+(lastUpdate.getUTCMonth()+1) : (lastUpdate.getUTCMonth()+1)}-${lastUpdate.getUTCDate() < 10 ? '0'+lastUpdate.getUTCDate() : lastUpdate.getUTCDate()}`;
        let todayString = `${today.getUTCFullYear()}-${(today.getUTCMonth()+1) < 10 ? '0'+(today.getUTCMonth()+1) : (today.getUTCMonth()+1)}-${today.getUTCDate() < 10 ? '0'+today.getUTCDate() : today.getUTCDate()}`
        let index = 0;
        let newRecords = 0;
        let nbProcessed = maxOAIPMHExports;

        let info = 'Update started';
        Logger.log('info', info);
        console.log(info);

        //Most of the time we cannot request all the documents in one batch, please fine tune the config regarding this value (maxOIAPMHExports)
        while (nbProcessed == maxOAIPMHExports) {
            //let updateUrl = oaipmhEndpoint+`/request?verb=ListRecords&resumptionToken=marcxml/${index}/${lastUpdateString}/${todayString}/`;
            let updateUrl = oaipmhEndpoint+`/request?verb=ListRecords&resumptionToken=marcxml/${index}/${lastUpdateString}/${todayString}/`;

            console.log(`Requesting ${updateUrl}`);
            Logger.log('info', `Requesting ${updateUrl}`);
            var data = {};
            try{
                data = await request(updateUrl)
                console.log('Data received')
                await new Promise(function(resolve, reject){
                        parseXmlString(data, async function (err, result) {
                            if(!err){
                                //If no new record, ListRecords[0] is empty
                                if(result['OAI-PMH'].ListRecords == undefined || result['OAI-PMH'].ListRecords[0].record == undefined){
                                    nbProcessed = maxOAIPMHExports-1;
                                    resolve();
                                    return;
                                }
                                let nbDocs = result['OAI-PMH'].ListRecords[0].record.length;
                                nbProcessed = nbDocs;
                                for (var index = 0; index < nbDocs; index++) {
                                    var element = result['OAI-PMH'].ListRecords[0].record[index];
                                    let record
                                    try {
                                        record = await self.parseRecordObject(element.metadata[0].record[0]);
                                        recordArray.push(record)
                                        newRecords++;
                                    } catch(err) {
                                        console.log(err)
                                    }
                                    //recordStream.push(record);
                                    //console.log(record)
                                }
                                resolve();
                            } else {
                                reject(err);
                            }
                        });
                    });
            } catch(e){
                    Logger.log('error',e);
                    console.error(e)
            }
            index += nbProcessed;
        }

        console.log(`Records updated : ${newRecords}`);
        //recordStream.push(null);

        
        updateLastUpdateFile();
        authorityManager.saveAuthorities();


        resolve(recordArray);
        
    }

    return new Promise(updatePromise);
}

/**
 * Extract and organize information from a record object previously parsed from MARCXML
 * 
 * This method contains helper functions *inside* of it : parsing MARCXML is no easy task
 * 
 * The treatment of MARCXML fields are configured in the main configuration file
 * 
 * @param {Object} record - The record object (parsed from MARCXML)
 * @returns {Promise}
 */
KohaImporter.prototype.parseRecordObject = function(record){

    //Helper functions
    //This function is a straight copy of retrieveTextFromMultipleSubfields with the only necessary
    //modification for being sure that what is recorded is "Prénom Nom", sorry
    var retriveAuthorNameSurnameFromMultipleSubfields = function(datafield, codes, separator) {
        var returnValue=null;
        let $a = '';
        let $b = '';
        separator = (separator === undefined) ? ' ' : separator;
        datafield.subfield.forEach(function(subfield){
            if(codes.indexOf(subfield.$.code) != -1){
                if(subfield._){
                    if(subfield.$.code === 'a') {
                        $a = subfield._.replace(/[\(\)\\\/,]/g,'');
                    } else if (subfield.$.code === 'b') {
                        $b = subfield._.replace(/[\(\)\\\/,]/g,'');
                    }
                }
            }
        });
        if( $a.length > 0 ||$b.length > 0) {
            returnValue = `${$b} ${$a}`;
        }
        return returnValue;
    }

    /**
     * This method is a copy of retrieveTextFromMultipleSubfields, adapted to retrieving a url 
     * 
     * @param {string} datafield The MARC tag we want to extract
     * @param {Array(string)} codes The codes from the aforementionned tag we want to extract
     * @param {string} separator The separator to be used between the extracted code (default to ' ')
     */
    var retrieveReadingUrlFromMultipleSubfields = function(datafield, codes, separator) {
        var returnValue=null;
        separator = (separator === undefined) ? ' ' : separator;
        datafield.subfield.forEach(function(subfield){
            if(codes.indexOf(subfield.$.code) != -1){
                if(subfield._){
                    if(returnValue===null){
                        returnValue = '';
                    } else {
                        returnValue += separator;
                    }
                    let cleanedString = subfield._ // .replace(/[\(\)\\\/,]/g,'');
                    returnValue += cleanedString;
                }
            }
        });
        return returnValue;
    }

    /**
     * This method is useful for getting a *single* string from several codes in a same MARC tag. For example this is useful
     * to get the name and surname of authors in a single string
     * 
     * @param {string} datafield The MARC tag we want to extract
     * @param {Array(string)} codes The codes from the aforementionned tag we want to extract
     * @param {string} separator The separator to be used between the extracted code (default to ' ')
     */
    var retrieveTextFromMultipleSubfields = function(datafield, codes, separator) {
        var returnValue=null;
        separator = (separator === undefined) ? ' ' : separator;
        datafield.subfield.forEach(function(subfield){
            if(codes.indexOf(subfield.$.code) != -1){
                if(subfield._){
                    if(returnValue===null){
                        returnValue = '';
                    } else {
                        returnValue += separator;
                    }
                    let cleanedString = subfield._.replace(/[\(\)\\\/,]/g,'');
                    returnValue += cleanedString;
                }
            }
        });
        return returnValue;
    }

    /**
     * This method is useful for getting *multiple* strings from several codes in a same MARC tag. Especialy useful for authorities
     * 
     * @param {string} datafield The MARC tag we wish to extract
     * @param {Array(string)} codes The interesting codes we wish to extract from the tag
     */
    var retrieveMultipleTextsFromMultipleSubfields = function(datafield, codes) {
        var returnValue=[];
        datafield.subfield.forEach(function(subfield){
            if(codes.indexOf(subfield.$.code) > -1){
                if(subfield._){
                    let cleanedString = subfield._.replace(/[\(\)\\\/,]/g,'');
                    returnValue.push(cleanedString);
                }
            }
        });
        return returnValue;
    }

    var getAuthorNameFromSubfields = function (datafield, tag, separator) {
        if(tag.tag.indexOf(datafield.$.tag) === -1){
            return null;
        }else {
            return retriveAuthorNameSurnameFromMultipleSubfields(datafield, tag.code, separator);
        }
    }

    var getReadingUrlFromSubfields = function (datafield, tag, separator) {
        if(tag.tag.indexOf(datafield.$.tag) === -1){
            return null;
        }else {
            return retrieveReadingUrlFromMultipleSubfields(datafield, tag.code, separator);
        }
    }

    var getTextFromSubfields = function (datafield, tag, separator) {
        if(tag.tag.indexOf(datafield.$.tag) === -1){
            return null;
        }else {
            return retrieveTextFromMultipleSubfields(datafield, tag.code, separator);
        }
    }

    var getMultipleTextsFromSubfields = function (datafield, tag) {
        if(tag.tag.indexOf(datafield.$.tag) === -1){
            return null;
        }else {
            return retrieveMultipleTextsFromMultipleSubfields(datafield, tag.code);
        }
    }

    var getSimplifiedPublicationDate = function (rawPublicationDate) {
        const regex = /[0-9]{4}/g;
        let m;
        let matchedDates = [];

        while ((m = regex.exec(rawPublicationDate)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            
            // The result can be accessed through the `m`-variable.
            m.forEach((match, groupIndex) => {
                matchedDates.push(match);
            });
        }

        if(matchedDates.length == 0) {
            return null;
        }

        matchedDates = matchedDates.map((stringDate) => {
            return Number(stringDate)
        });

        return `${Math.max(...matchedDates)}`;
    }

    //Core method
    return new Promise(function(resolve, reject){
        if(record != null){
            //console.log(result);
            let id = null;
            let isbn = null;        //010a
            let issn = null;        //011a
            let ismn = null;        //013a
            let ean = null;         //073a
            let title = null;       //200a
            let editor = null;      //210c
            let datePub = null;     //210d                  -- rarely clean
            let nbPages = null;     //215a                  -- rarely clean
            let note = null;        //300a
            let noteBioIndex = null;//320a
            let thesisInfo = null;  //328abcde
            let thesisUrl = null;   //328u
            let description = null; //330a
            let table = [];         //359b
            let collection = null;  //410t
            let uniformTitle = null;//500a
            let formTitle = null;   //503a
            let authorities = [];   //600-607,616ajxy   -- several
            let mainAuthorities = [];
            let freeIndex = [];     //610[a]          -- several
            let cdu = [];           //675[a]          -- several
            let dewey = null;       //676a N
            let lcc = null;         //680a+b
            let otherClass = null;  //686a+2
            let authors = [];       //700a+b,701a+b,702a+b,710a+b,711a+b,712a+b  -- can be of multiple occurence
            let readingUrl = null;  //856u


            record.datafield.forEach(function(datafield){
                
                let authorityObject = {
                    sub : [],
                    geo : [],
                    form : []
                };

                let tmp_isbn = getTextFromSubfields(datafield, kohaTags.isbn);
                isbn = tmp_isbn ? tmp_isbn : isbn;

                let tmp_issn = getTextFromSubfields(datafield, kohaTags.issn);
                issn = tmp_issn ? tmp_issn : issn;

                let tmp_ismn = getTextFromSubfields(datafield, kohaTags.ismn);
                ismn = tmp_ismn ? tmp_ismn : ismn;

                let tmp_ean = getTextFromSubfields(datafield, kohaTags.ean);
                ean = tmp_ean ? tmp_ean : ean;

                let tmp_title = getTextFromSubfields(datafield, kohaTags.title);
                title = tmp_title ? tmp_title : title;

                let tmp_editor = getTextFromSubfields(datafield, kohaTags.editor);
                editor = tmp_editor ? tmp_editor : editor;

                //Here we record only the highest 4 digits found in the field
                let tmp_datePub = getSimplifiedPublicationDate(getTextFromSubfields(datafield, kohaTags.datePub));
                datePub = tmp_datePub ? tmp_datePub : datePub;

                let tmp_nbPages = getTextFromSubfields(datafield, kohaTags.nbPages);
                nbPages = tmp_nbPages ? tmp_nbPages : nbPages;

                let tmp_note = getTextFromSubfields(datafield, kohaTags.note);
                note = tmp_note ? tmp_note : note;

                let tmp_noteBioIndex = getTextFromSubfields(datafield, kohaTags.noteBioIndex);
                noteBioIndex = tmp_noteBioIndex ? tmp_noteBioIndex : noteBioIndex;

                let tmp_thesisInfo = getTextFromSubfields(datafield, kohaTags.thesisInfo);
                thesisInfo = tmp_thesisInfo ? tmp_thesisInfo : thesisInfo;

                let tmp_thesisUrl = getTextFromSubfields(datafield, kohaTags.thesisUrl);
                thesisUrl = tmp_thesisUrl ? tmp_thesisUrl : thesisUrl;

                let tmp_description = getTextFromSubfields(datafield, kohaTags.description);
                description = tmp_description ? tmp_description : description;

                let tmp_table = getMultipleTextsFromSubfields(datafield, kohaTags.table);
                Array.prototype.push.apply(table, tmp_table);

                let tmp_collection = getTextFromSubfields(datafield, kohaTags.collection);
                collection = tmp_collection ? tmp_collection : collection;

                let tmp_uniformTitle = getTextFromSubfields(datafield, kohaTags.uniformTitle);
                uniformTitle = tmp_uniformTitle ? tmp_uniformTitle : uniformTitle;

                let tmp_formTitle = getTextFromSubfields(datafield, kohaTags.formTitle);
                formTitle = tmp_formTitle ? tmp_formTitle : formTitle;

                let tmp_mainAuthority = getTextFromSubfields(datafield, kohaTags.mainAuthority);
                if(tmp_mainAuthority){
                    authorityObject.main = tmp_mainAuthority;
                    mainAuthorities.push(tmp_mainAuthority);
                }

                let tmp_subAuthority = getMultipleTextsFromSubfields(datafield, kohaTags.subAuthority);
                Array.prototype.push.apply(authorityObject.sub, tmp_subAuthority);

                let tmp_geoAuthority = getMultipleTextsFromSubfields(datafield, kohaTags.geoAuthority);
                Array.prototype.push.apply(authorityObject.geo,tmp_geoAuthority);

                let tmp_dateAuthority = getTextFromSubfields(datafield, kohaTags.dateAuthority);
                tmp_dateAuthority ? authorityObject.date = tmp_dateAuthority : null;

                let tmp_formAuthority = getMultipleTextsFromSubfields(datafield, kohaTags.formAuthority);
                Array.prototype.push.apply(authorityObject.form, tmp_formAuthority);

                let tmp_freeIndex = getMultipleTextsFromSubfields(datafield, kohaTags.freeIndex);
                Array.prototype.push.apply(freeIndex, tmp_freeIndex);

                let tmp_cdu = getTextFromSubfields(datafield, kohaTags.cdu);
                tmp_cdu ? cdu.push(tmp_cdu) : null;
                
                let tmp_dewey = getTextFromSubfields(datafield, kohaTags.dewey);
                dewey = tmp_dewey ? tmp_dewey : dewey;

                let tmp_lcc = getTextFromSubfields(datafield, kohaTags.lcc);
                lcc = tmp_lcc ? tmp_lcc : lcc;

                let tmp_otherClass = getTextFromSubfields(datafield, kohaTags.otherClass);
                otherClass = tmp_otherClass ? tmp_otherClass : otherClass;

                let tmp_author = getAuthorNameFromSubfields(datafield, kohaTags.authors);
                tmp_author ? authors.push(tmp_author) : null;

                let tmp_readingUrl = getReadingUrlFromSubfields(datafield, kohaTags.readingUrl);
                readingUrl = tmp_readingUrl ? tmp_readingUrl : readingUrl;

                //If authorityObject has main, we add it to authorities
                if(authorityObject.main){
                    authorities.push(authorityObject);
                    //Here we should add content of auhtorityObject to Authority module
                }
            });

            //Remove duplicates from authorities
            /*
            let authorities_uniq = authorities.filter(function(elem, index,self){
                return index==self.indexOf(elem);
            });
            */

            //Find Koha id of the record
            record.controlfield.forEach(function(controlfield){
                if(controlfield.$.tag == '001'){
                    id = controlfield._;
                }
            });

            if(id == null){
                reject('No ID found.');
                return;
            }

            //Reference record in authority tree
            authorityManager.addBook(id, authorities);


            var bibliodocument = {
                id : id,
                isbn : isbn,
                issn : issn,
                ismn : ismn,
                ean : ean,
                title : title,
                editor : editor,
                datePub : datePub,
                nbPages : nbPages,
                note : note,
                noteBioIndex : noteBioIndex,
                thesisInfo : thesisInfo,
                thesisUrl : thesisUrl,
                description : description,
                table : table,
                collection : collection,
                uniformTitle : uniformTitle,
                formTitle : formTitle,
                authorities : authorities,
                mainAuthorities : mainAuthorities,
                freeIndex : freeIndex,
                cdu : cdu,
                dewey : dewey,
                lcc : lcc,
                otherClass : otherClass,
                authors : authors,
                readingUrl : readingUrl
            }

            //Do not download covers yet - Do downlaod
            if(config.get('Bibliotouch.downloadCovers')){
                coverDownloader.dlCover(bibliodocument);
            }

            bibliodocument.normalizedWords = getNormalizedWords(bibliodocument);

            resolve(bibliodocument);
        } else {
            let cause = 'Marcxml is null';
            Logger.log('info',cause);
            reject(cause);
        }
    });
}

module.exports = KohaImporter;