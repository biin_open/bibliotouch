var Sequelize = require('sequelize');
var kohaDb = require('./kohaDb');

/**
 * DB Schema creation for [Sequelize](http://docs.sequelizejs.com/)
 * 
 * Here we only focus on the bilioitems table for the initial import.
 * 
 * *EDIT : Ok, now it is not the biblioitems table anymore, but the biblio_metadata, works somewhat similarly for us, keeping old names of variables because I am not paid enough for this shit.*
 */

var BiblioItems = kohaDb.define('biblio_metadata', {
  biblioitemnumber: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      field: 'id'
  },
  biblionumber: {
      type: Sequelize.INTEGER,
      field: 'biblionumber'
  },
  timestamp: {
      type: Sequelize.DATE,
      field: 'timestamp'
  },
  marcxml: {
      type: Sequelize.TEXT,
      field: 'metadata'
  }
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});

BiblioItems.removeAttribute('createdAt');
BiblioItems.removeAttribute('updatedAt');



module.exports = BiblioItems;