/**
 * Express controller returning the theme list and the books for a given theme
 * 
 * This is the interface for the Themes module. Each time there is a request, this controller
 * get an Array of ids of book. Then the controller gets all the objects [by id from search-index](https://github.com/fergiemcdowall/search-index/blob/master/docs/API.md#get)
 * 
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module themesController
 */

var express = require('express');
var sw = require('stopword');
var Logger = require('../helpers/logger');
var Themes = require('../models/themes');
var index = require('../helpers/searchIndex');
var authorities = require('../models/authorities');

var router = express.Router();

router.get('/', function(req, res){
    res.type('application/json');
    res.send(JSON.stringify(Themes.getThemes()));
});

router.get('/:theme', function(req, res){
    res.type('application/json');
    if(Themes.getThemes()[req.params.theme] != null){
        res.send(JSON.stringify(Themes.getThemes()[req.params.theme]));
    } else {
        res.send(JSON.stringify(Themes.getEmptyTheme()));
    }
});

router.get('/:theme/books-objects', function(req, res){
    function uniq_fast(a) {
        var seen = {}
        var out = []
        var len = a.length
        var j = 0
        for(var i = 0; i < len; i++) {
             var item = a[i]
             if(seen[item] !== 1) {
                   seen[item] = 1
                   out[j++] = item
             }
        }
        return out
    }

    res.type('application/json');
    if(Themes.getThemes()[req.params.theme] != null){
        let idsToRetrieve = [];
        Themes.getThemes()[req.params.theme].vedettes.forEach(function(vedette) {
            Array.prototype.push.apply(idsToRetrieve, authorities.getAuthorities()[vedette].books)
        });

        let uniqueIds = uniq_fast(idsToRetrieve)

        index.get(uniqueIds)
                .then(function(books){
                    res.send(JSON.stringify(books));
                });
    } else {
        res.send(JSON.stringify([]));
    }
});

router.get('/:theme/books', function(req, res){
    res.type('application/json');
    if(Themes.getThemes()[req.params.theme] != null){
        let idsToRetrieve = [];
        let objectifiedIds = [];

        Themes.getThemes()[req.params.theme].vedettes.forEach(function(vedette) {
            Array.prototype.push.apply(idsToRetrieve, authorities.getAuthorities()[vedette].books)
        });

        let uniqueIds = idsToRetrieve.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        })

        for(const idstring of uniqueIds) {
            objectifiedIds.push({id:idstring});
        }

        res.send(JSON.stringify(objectifiedIds));
    } else {
        res.send(JSON.stringify([]));
    }
});

module.exports = router;