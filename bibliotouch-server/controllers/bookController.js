/**
 * Express controller to retrieve one book object from its id
 * 
 * Each time there is a request, this controller
 * get an Array of ids of book. Then the controller gets all the objects [by id from search-index](https://github.com/fergiemcdowall/search-index/blob/master/docs/API.md#get)
 * 
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module themesController
 */

var express = require('express');
var Logger = require('../helpers/logger');
var index = require('../helpers/searchIndex');

var router = express.Router();

router.get('/', function(req, res){
    res.type('application/json');
    res.send(JSON.stringify({}));
});

router.get('/:bookid', function(req, res){
    res.type('application/json');
    if(req.params.bookid != null){
        index.get([req.params.bookid])
                .then(function(books){
                    res.send(JSON.stringify(books));
                });
    } else {
        res.send(JSON.stringify([]));
    }
});


module.exports = router;