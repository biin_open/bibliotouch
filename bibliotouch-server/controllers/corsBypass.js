/**
 * Express controller requesting Koha REST API endpoint without CORS complication
 * 
 * In my use case, it seems that the OAIPMH endpoint doesn't have Allow-Access-Control-Header setup,
 * this is just a proxy.
 * 
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module corsBypass
 */

var express = require('express');
var Logger = require('../helpers/logger');
var bodyParser = require('body-parser');
var request = require('request-promise-native');
var config = require('config');
var parseXmlString = require('xml2js').parseString;


var ilsdi = config.get('Bibliotouch.koha.ilsdi');
var locationcode = config.get('Bibliotouch.locationcode');

var jsonParser = bodyParser.json();

var router = express.Router();

router.get('/:query',jsonParser, function(req, res){
    let translateLocationCode = function (code) {
        if(locationcode[code]) {
            return locationcode[code]
        }
        return code
    }
    request(ilsdi + '?service=GetRecords&id=' + req.params.query)
        .then(function(body){
            parseXmlString(body, function (err, result) {
                if(!err){
                  //If no new record, ListRecords[0] is empty
                  if(result['GetRecords'].record[0].code == "RecordNotFound"){
                      throw 'Error while getting record';
                  } else if(result['GetRecords'].record[0].items && result['GetRecords'].record[0].items.length > 0) {
                    let jsonobj = {
                      exemplaires: []
                    };
                    for(const item of result['GetRecords'].record[0].items) {
                        let itemObj = {
                            itemcallnumber: item.item[0].itemcallnumber[0],
                            notforloan: item.item[0].notforloan[0],
                            location: translateLocationCode(item.item[0].location[0])
                        };

                        if(item.item[0].onloan) {
                            itemObj.onloan = item.item[0].onloan[0]
                        }

                        jsonobj.exemplaires.push(itemObj);
                    }
                    res.contentType('application/json');
                    res.send(jsonobj);
                  }
                }
            });
        }).catch(function (e) {
            console.error(`Erreur ${e.statusCode} : ${e}`);
        });
});

module.exports = router;