/**
 * Express controller returning search results.
 * 
 * We have to provide GET and POST because some libraries don't allow a body in a GET request.
 * 
 * Makes use of search-index [search functionnality](https://github.com/fergiemcdowall/search-index/blob/master/docs/API.md#search)
 * 
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module search
 */

var express = require('express');
var Logger = require('../helpers/logger');
var index = require('../helpers/searchIndex');
var bodyParser = require('body-parser');
var jpv = require('jpv');

var jsonParser = bodyParser.json();

var router = express.Router();

var pattern = {
    query: [
        {
            AND : {},
            NOT : {}
        }
    ],
    pageSize: '(number)'
}

router.get('/:query',jsonParser, function(req, res){
    let queryObject = {};
    
    if(req.body.query){
        queryObject.query = JSON.parse(req.body.query);
    } else {
        queryObject.query = {
            AND : { '*' : [req.params.query] }
        };
    }
    
    queryObject.pageSize = 400;

    if(jpv.validate(queryObject, pattern, true)){
        index.search(queryObject)
        .then(function(results){
            res.json(results);
        });
    } else {
        res.json([]);
    }
});

router.post('/',jsonParser, function(req, res){
    let queryObject = {};
    
    if(req.body.query){
        queryObject.query = JSON.parse(req.body.query);
    } else {
        queryObject.query = {
            AND : { '*' : [req.params.query] }
        };
    }
    
    queryObject.pageSize = 4000;

    if(jpv.validate(queryObject, pattern, true)){
    index.search(queryObject)
        .then(function(results){
            res.json(results);
        });
    } else {
        res.json([]);
    }
});

module.exports = router;