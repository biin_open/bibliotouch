/**
 * Express controller sending email to users with the list of their favs
 * 
 * 
 * @author Alix Ducros <ducrosalix@hotmail.fr>
 * @module corsBypass
 */

var express = require('express');
var Logger = require('../helpers/logger');
var bodyParser = require('body-parser');
var config = require('config');
var nodemailer = require('nodemailer');

let sentMail = {}

var jsonParser = bodyParser.json();

var router = express.Router();

function booksToHtml (books) {
  let res = ''
  for (const book of books) {
    let url = ''
    if (book.readingUrl) {
      url = `<p><a href="${book.readingUrl}">Lire le document en ligne</a></p>`
    }

    res += `
    <div>
      <b>${book.title}</b>
      <em>${book.authors[0]}</em>
      <p>Cote : ${book.callNumber} (${book.location}), Disponibilité : ${book.available}</p>
      <p>${book.description ? book.description : ''}</p>
      ${url}
    </div>
  `
  }
  return res
}

//Reset the rate limiting every 24h
setInterval(()=> {
  sentMail = {}
}, 24*60*60*1000)

function updateRateLimiting (email) {
  if(sentMail[email] != undefined) {
    sentMail[email] += 1
  } else {
    sentMail[email] = 0
  }
}

router.post('/',jsonParser, function(req, res){
  let mailData = req.body
  let transporter = null

  if(config.get('Bibliotouch.mail.smtp')) {
    transporter = nodemailer.createTransport({
        host: config.get('Bibliotouch.smtp.smtphost'),
        port: config.get('Bibliotouch.smtp.port'),
        secure: config.get('Bibliotouch.smtp.secure'),
        auth: {
            user: config.get('Bibliotouch.smtp.user'),
            pass: config.get('Bibliotouch.smtp.password')
        }
    });
  } else if(config.get('Bibliotouch.mail.sendmail')) {
    transporter = nodemailer.createTransport({
      sendmail: true,
      newline: config.get('Bibliotouch.sendmail.newline'),
      path: config.get('Bibliotouch.sendmail.path')
    })
  } else { 
    res.statusCode = 500;
    res.send('')
    return
  }

  // setup email data with unicode symbols
  let mailOptions = {
      from: config.get('Bibliotouch.mail.fromfield'),
      to: mailData.address, // list of receivers
      subject: config.get('Bibliotouch.mail.subject'),
      //text: booksToHtml(mailData.books)
      html:  booksToHtml(mailData.books)
  };

  // send mail with defined transport object if rate ok
  if(!sentMail[mailData.address] || sentMail[mailData.address] < 10) {
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          res.statusCode = 500;
          res.send('')
            return console.log(error);
        }
        updateRateLimiting(mailData.address)
        res.statusCode = 200;
        res.send('')
        Logger.log('info', `Mail sent to : ${mailData.address} : ${mailData.books.length} documents.`)
    });
  } else {
    res.statusCode = 500;
    res.send('')
  }
});

module.exports = router;