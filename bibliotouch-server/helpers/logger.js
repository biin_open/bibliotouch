var winston = require('winston');

winston.configure({
    transports: [
      new (winston.transports.File)({ filename: 'bibliotouch-logs.log' })
    ]
  });

module.exports = winston;