var express = require('express');
var cors = require('cors')
var compression = require('compression');
var controllers = require('./controllers');
var KohaImporter = require('./importers/kohaImporter/kohaImporter');
var indexer = require('./helpers/searchIndex'); 
var Logger = require('./helpers/logger');
var parseArgs = require('minimist');
var schedule = require('node-schedule');
var config = require('config');
var fs = require('fs');
var bodyParser = require('body-parser');


var argv = parseArgs(process.argv.slice(2));


var app = express();
var serverPort = argv.port ? argv.port : config.get('Bibliotouch.serverPort');

app.use((req, res, next) => {
    // Here we try to filter requests for cover pictures : there are too many of them and it pollutes the logs
    if(!req.originalUrl.match(/.*\.jpg/)){
        Logger.log('info', `${req.originalUrl} - ${req.ip}`);
    }
    next()
})

app.use(compression());
app.use(cors())
app.use(controllers);
app.use('/', express.static('../bibliotouch-front'));
app.use('/covers', express.static('./covers'));
app.use(bodyParser.json()); // for parsing application/json


//Initialize data directory
fs.mkdir('data', err => console.log('Did not create "data" directory'));

var importer;
if(argv.adapter == 'koha'){
    importer = new KohaImporter();
}else {
    //By default we return KohaImporter
    importer = new KohaImporter();
}

async function flush(){
    await indexer.flushIndex()
        .then(function(){
            //Import records from SIGB
            return importer.import();
        }).then(function(stream){
            return indexer.fillIndexFromStream(stream);
        })
}

if(argv.flush){
    flush().then(function(){
        return importer.update().then(function(array){
            return indexer.fillIndexFromArray(array);
        }).catch((e)=>{
            console.log(e)
        });
    })
} else {
    //Start update and then schedule updates
    importer.update().then(function(array){
        return indexer.fillIndexFromArray(array);
    }).then(function () {
        let cron = config.get('Bibliotouch.updateTime.cron');
        schedule.scheduleJob(cron, function(){
            importer.update().then(function(array){
                return indexer.fillIndexFromArray(array);
            });
        })
    }).catch((e)=>{
        console.log(e)
    });
    indexer.initIndex();
    //Start server app
    app.listen(serverPort, function () {
        console.log(`Server running on port ${serverPort}`);
    });
}

module.exports = app;